/**
 ******************************************************************************
 * @file    udp_echoserver.c
 * @author  MCD Application Team
 * @version V1.1.0
 * @date    31-July-2013
 * @brief   UDP echo server
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; COPYRIGHT 2013 STMicroelectronics</center></h2>
 *
 * Licensed under MCD-ST Liberty SW License Agreement V2, (the "License");
 * You may not use this file except in compliance with the License.
 * You may obtain a copy of the License at:
 *
 *        http://www.st.com/software_license_agreement_liberty_v2
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 ******************************************************************************
 */

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "lwip/pbuf.h"
#include "lwip/udp.h"
#include "lwip/tcp.h"
#include <string.h>
#include <stdio.h>
#include "serial_debug.h"

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
#define UDP_SERVER_PORT    7   /* define the UDP local connection port */
#define UDP_CLIENT_PORT    7   /* define the UDP remote connection port */

#define MAX_SEND_LEN		1000
#define HEAD_PACKET_LEN		10
#define TARGET_BUFFER		frame_buffer

/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
char m_server_state = 0;

unsigned int g_frame_total_len;
unsigned short g_cur_section = 0;
unsigned short g_total_section;

char *gp_packet;
// uint16_t test_buffer[100];
u8_t data[100];
__IO uint32_t message_count;

/* Private function prototypes -----------------------------------------------*/
void udp_echoserver_receive_callback(void *arg, struct udp_pcb *upcb,
		struct pbuf *p, struct ip_addr *addr, u16_t port);
void set_status(struct udp_pcb *upcb, char a_status);
void Init_image_send_process();


/* Private functions ---------------------------------------------------------*/

/**
 * @brief  Initialize the server application.
 * @param  None
 * @retval None
 */
void udp_echoserver_init(void) {
	struct udp_pcb *upcb;
	err_t err;
	struct pbuf *p; //jini

	/* Create a new UDP control block  */
	upcb = udp_new();

	if (upcb) {
		/* Bind the upcb to the UDP_PORT port */
		/* Using IP_ADDR_ANY allow the upcb to be used by any local interface */
		err = udp_bind(upcb, IP_ADDR_ANY, UDP_SERVER_PORT);
		message_count = UN_CONNECT;
		printf("[S] : bind success\n");
		if (err == ERR_OK) {
			/* Set a receive callback for the upcb */
			udp_recv(upcb, udp_echoserver_receive_callback, NULL);
		} else {
			udp_remove(upcb);
			printf("can not bind pcb");
		}
	} else {
		printf("can not create pcb");
	}
}

/**
 * @brief This function is called when an UDP datagrm has been received on the port UDP_PORT.
 * @param arg user supplied argument (udp_pcb.recv_arg)
 * @param pcb the udp_pcb which received data
 * @param p the packet buffer that was received
 * @param addr the remote IP address from which the packet was received
 * @param port the remote port from which the packet was received
 * @retval None
 */
void udp_echoserver_receive_callback(void *arg, struct udp_pcb *upcb,
		struct pbuf *p, struct ip_addr *addr, u16_t port) {
	char *recv_data = p->payload;
	struct pbuf *new_buf;

	printf("[S] receive : %s\n", recv_data);

	// Must connect to the remote client
	udp_connect(upcb, addr, UDP_CLIENT_PORT);

	switch (m_server_state) {

	case UN_CONNECT:
		if (0 == memcmp(recv_data, "CONNECT", sizeof("CONNECT"))) {
			m_server_state = CONNECT;
			printf("[S] 1st send \"%s\"\n", p->payload);

			/* Tell the client that we have accepted it */
			udp_send(upcb, p);	//, addr, UDP_CLIENT_PORT
			printf("Let's go!!\n");

		} else {
			set_status(upcb, UN_CONNECT);
		}
		break;
	case CONNECT:
		if (0 == memcmp(recv_data, "CAMERA_ON", sizeof("CAMERA_ON"))) {
			m_server_state = CAMERA_ON;

			CameraOn();
			Capture_QQVGA_Image();
			Init_image_send_process();

			udp_send(upcb, p);
			printf("[S] 2nd send CAMERA_ON\n");

			/*
			 memcpy(data, "IMAGE_SEND_START", sizeof("IMAGE_SEND_START"));
			 new_buf = pbuf_alloc(PBUF_TRANSPORT, sizeof(data), PBUF_RAM);
			 pbuf_take(new_buf, data, sizeof(data));
			 udp_send(upcb, new_buf);
			 print_RGB(0, 10);
			 */
		} else {
			set_status(upcb, UN_CONNECT);
		}
		break;
	case CAMERA_ON:
		if (!memcmp(recv_data, "REQUEST_IMAGE", sizeof("REQUEST_IMAGE"))) {
			new_buf = pbuf_alloc(PBUF_TRANSPORT, MAX_SEND_LEN + HEAD_PACKET_LEN, PBUF_RAM);
			char *p_pos = gp_packet;
			memcpy(p_pos, "JHP", 3);
			p_pos += 3;
			unsigned short img_data_send_len;

			// sections that length is MAX_SEND_LEN
			if(g_cur_section < g_total_section -1 ||
					(g_cur_section == g_total_section -1 && (g_frame_total_len % MAX_SEND_LEN) == 0)){
				// Length of IMG data
				img_data_send_len = MAX_SEND_LEN;
				*(unsigned short *)(p_pos) = img_data_send_len ^ 0xffff; // 3E8

				// Currunt section in total section
				p_pos += 2;
				*(unsigned short *)(p_pos) = g_cur_section ^ 0xffff;
				printf("cur %d section >> 0x%x, 0x%x\n", g_cur_section, *p_pos, *(p_pos + 1)); // 0x08 0xfc
				p_pos += 2;

				// total section of 1 frame
				*(unsigned short *)(p_pos) = g_total_section^ 0xffff;
				printf("total %d section >> 0x%x, 0x%x\n", g_total_section, *p_pos, *(p_pos + 1)); // 0x08 0xff
				p_pos += 2;

				// IMG data
				memcpy(p_pos, TARGET_BUFFER + MAX_SEND_LEN * g_cur_section /2, MAX_SEND_LEN);
				*(p_pos + MAX_SEND_LEN) = NULL;
				int i;
				for(i = 0; i < 20; i++){
					printf("%02x", *(9 + gp_packet +i));
				}
				printf("\n packet size: %d\n", MAX_SEND_LEN + HEAD_PACKET_LEN);

				g_cur_section++;

			}else { // if this is Last section and has length lower than MAX_SEND_LEN
				// Length of IMG data
				img_data_send_len = g_frame_total_len % MAX_SEND_LEN;
				*(unsigned short *)(p_pos) = img_data_send_len ^ 0xffff; // 3E8

				// Currunt section in total section
				p_pos += 2;
				*(unsigned short *)(p_pos) = g_cur_section ^ 0xffff;
				printf("cur %d section >> 0x%x, 0x%x\n", g_cur_section, *p_pos, *(p_pos + 1)); // 0x08 0xfc
				p_pos += 2;

				// total section of 1 frame
				*(unsigned short *)(p_pos) = g_total_section^ 0xffff;
				printf("total %d section >> 0x%x, 0x%x\n", g_total_section, *p_pos, *(p_pos + 1)); // 0x08 0xff
				p_pos += 2;

				memcpy(p_pos, TARGET_BUFFER + MAX_SEND_LEN * g_cur_section /2, img_data_send_len);
				*(p_pos + img_data_send_len) = NULL;

				g_cur_section = 0;
				m_server_state = SEND_STOP;
				Capture_QQVGA_Image();
				LCD_Clear(0x07E0);
				LCD_DrawCircle(80, 60, 10);
			}

			// Copy packet to new buffer
			pbuf_take(new_buf, gp_packet, img_data_send_len + HEAD_PACKET_LEN);

			printf("new buf : %b", new_buf->payload);

			// Send new buf using udp
			udp_send(upcb, new_buf);

			pbuf_free(new_buf);
		}

		break;
	case SEND_STOP:

		break;
	default:
		break;

	}
	/* Free the p buffer */
	if (NULL != p)
		pbuf_free(p);

}


/*
 * Initial variable related receiving image
 */
void Init_image_send_process() {

	g_frame_total_len = sizeof(TARGET_BUFFER);
	g_cur_section = 0;
	g_total_section = g_frame_total_len / MAX_SEND_LEN; //39

	if (0 != g_frame_total_len % MAX_SEND_LEN)
		g_total_section++;
	if(NULL == gp_packet) gp_packet = (char *)malloc(MAX_SEND_LEN + HEAD_PACKET_LEN);
}

/*
 * set m_server_status
 * param upcb the handle of udp
 * param a_status the status that u want to set like UN_CONNECT or ...
 */
void set_status(struct udp_pcb *upcb, char a_status) {
	if (UN_CONNECT == a_status) {
		m_server_state = UN_CONNECT;
		/* free the UDP connection, so we can accept new clients */
		udp_remove(upcb);
		printf("[C] : UN_CONNECT\n\n\n");
		if(NULL != gp_packet) free(gp_packet);
	}
}


/*
 * print m_server_state to USART
 */
void print_status() {
	switch (m_server_state) {
	case UN_CONNECT:
		printf("[C] : UN_CONNECT\n");
		break;
	case CONNECT:
		printf("[C] : CONNECT\n");
		break;
	case CAMERA_ON:
		printf("[C] : CAMERA_ON\n");
		break;
	default:
		break;
	}
}

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
