/**
 ******************************************************************************
 * @file    main.c
 * @author  MCD Application Team
 * @version V1.1.0
 * @date    31-July-2013
 * @brief   Main program body
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; COPYRIGHT 2013 STMicroelectronics</center></h2>
 *
 * Licensed under MCD-ST Liberty SW License Agreement V2, (the "License");
 * You may not use this file except in compliance with the License.
 * You may obtain a copy of the License at:
 *
 *        http://www.st.com/software_license_agreement_liberty_v2
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 ******************************************************************************
 */

/* Includes ------------------------------------------------------------------*/
#include "stm32f4x7_eth.h"
#include "netconf.h"
#include "main.h"
#include "udp_echoserver.h"
#include "serial_debug.h"
#include "stm324xg_eval.h"
/*
 * Camera
 */
#include "dcmi_ov9655.h"
#include "dcmi_ov2640.h"

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
#define SYSTEMTICK_PERIOD_MS  10

/*--------------- LCD Messages ---------------*/
#if defined (STM32F40XX)
#define MESSAGE1   "    STM32F40/41x     "
#elif defined (STM32F427X)
#define MESSAGE1   "     STM32F427x      "
#endif
#define MESSAGE2   "     UDP server     "
#define MESSAGE3   "    Image Sender     "
#define MESSAGE4   "   wait connection   "

/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
__IO uint32_t LocalTime = 0; /* this variable is used to create a time reference incremented by 10ms */
uint32_t timingdelay;

/*
 * Camera
 */

RCC_ClocksTypeDef RCC_Clocks;
OV9655_IDTypeDef OV9655_Camera_ID;
OV2640_IDTypeDef OV2640_Camera_ID;

__IO uint16_t uhADCVal = 0;
uint8_t abuffer[40];

extern Camera_TypeDef Camera;
extern ImageFormat_TypeDef ImageFormat;
extern __IO uint8_t ValueMax;
extern const uint8_t * ImageForematArray[];

/* Private function prototypes -----------------------------------------------*/

/* Private functions ---------------------------------------------------------*/

// RCC_ClocksTypeDef RCC_Clocks;
/**
 * @brief  Main program.
 * @param  None
 * @retval None
 */
int main(void) {
	/*!< At this stage the microcontroller clock setting is already configured to
	 168 MHz, this is done through SystemInit() function which is called from
	 startup file (startup_stm32f4xx.s) before to branch to application main.
	 To reconfigure the default setting of SystemInit() function, refer to
	 system_stm32f4xx.c file
	 */
	// RCC_GetClocksFreq(&RCC_Clocks);
	// SysTick_Config(RCC_Clocks.HCLK_Frequency / 100);
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_4);

#ifdef SERIAL_DEBUG
	DebugComPort_Init();
#endif

	/* configure ethernet */
	ETH_BSP_Config();

	printf2("Ethernet Part Start! \n");
	/*Initialize LCD and Leds */
	LCD_LED_Init();

	/* Initilaize the LwIP stack */
	LwIP_Init();

	/* UDP echoserver */
	udp_echoserver_init();

	/* Infinite loop */
	while (1) {
		/* check if any packet received */
		if (ETH_CheckFrameReceived()) {
			/* process received ethernet packet */
			LwIP_Pkt_Handle();
		}
		/* handle periodic timers for LwIP */
		LwIP_Periodic_Handle(LocalTime);
	}

}

/**
 * @brief  Inserts a delay time.
 * @param  nCount: number of 10ms periods to wait for.
 * @retval None
 */
void Delay(uint32_t nCount) {
	/* Capture the current local time */
	timingdelay = LocalTime + nCount;

	/* wait until the desired delay finish */
	while (timingdelay > LocalTime) {
	}
}

/**
 * @brief  Updates the system local time
 * @param  None
 * @retval None
 */
void Time_Update(void) {
	LocalTime += SYSTEMTICK_PERIOD_MS;
}


void CameraOn() {
	LCD_Init();
	LCD_Clear(Yellow);
	LCD_SetTextColor(White);

	// ADC configuration
	ADC_Config();

	// Initializes the DCMI interface (I2C and GPIO) used to configure the camera
	OV2640_HW_Init();

	// Read the OV2640 Manufacturer identifier
	OV2640_ReadID(&OV2640_Camera_ID);

	if (OV2640_Camera_ID.PIDH == 0x26) {
		Camera = OV2640_CAMERA;
		sprintf((char*) abuffer, "OV2640 Camera ID 0x%x",
				OV2640_Camera_ID.PIDH);
		ValueMax = 2;
	} else {
		LCD_SetTextColor(LCD_COLOR_RED);
		LCD_DisplayStringLine(LINE(4),
				(uint8_t*) "Check the Camera HW and try again");
		while (1)
			;
	}

	Delay(200);

	// Initialize ImageFormat as QQVGA
	ImageFormat = 0;// (ImageFormat_TypeDef) Demo_Init();


	// Configure the Camera module mounted on STM324xG-EVAL/STM324x7I-EVAL boards
	// Demo_LCD_Clear();
	// LCD_DisplayStringLine(LINE(4), (uint8_t*) "Camera Init..               ");
	Camera_Config();

	sprintf((char*) abuffer, " Image selected: %s",
			ImageForematArray[ImageFormat]);
	LCD_DisplayStringLine(LINE(4), (uint8_t*) abuffer);

}


void Capture_QQVGA_Image() {
	// Enable DMA2 stream 1 and DCMI interface then start image capture
	DMA_Cmd(DMA2_Stream1, ENABLE);
	//
	DCMI_Cmd(ENABLE);

	// Insert 100ms delay: wait 100ms
	Delay(200);

	DCMI_CaptureCmd(ENABLE);

	LCD_ClearLine(LINE(4));
	Demo_LCD_Clear();

	// LCD Display window
	LCD_SetDisplayWindow(179, 239, 120, 160);
	LCD_WriteReg(LCD_REG_3, 0x1038);
	LCD_WriteRAM_Prepare();

}

void print_RGB(int start_pixel, int dest_pixel) {
	printf2("\n**********************\n print_RGB \n**********************\n");
	char r = 0, g = 0, b = 0;
	uint16_t pixel;

	if (start_pixel >= 0 && dest_pixel <= 120 * 160
			&& (dest_pixel - start_pixel) > 0) {
		int i;
		for (i = start_pixel; i < dest_pixel; i++) {
			pixel = frame_buffer[i];
			r = (pixel & 0xF800) >> 11;
			g = (pixel & 0x7E0) >> 5;
			b = pixel & 0x001F;
			printf2("%d.%d.%d/", r, g, b);
			if (!(i % 3))
				printf2("\n");
		}
		Delay(600);
	}
}

/**
 * @brief  Initializes the LCD and LEDs resources.
 * @param  None
 * @retval None
 */
void LCD_LED_Init(void) {
#ifdef USE_LCD
	/* Initialize the STM324xG-EVAL's LCD */
	STM324xG_LCD_Init();
#endif

	/* Initialize STM324xG-EVAL's LEDs */
	STM_EVAL_LEDInit(LED1);
	STM_EVAL_LEDInit(LED2);
	STM_EVAL_LEDInit(LED3);
	STM_EVAL_LEDInit(LED4);

	/* Leds on */
	STM_EVAL_LEDOn(LED1);
	STM_EVAL_LEDOn(LED2);
	STM_EVAL_LEDOn(LED3);
	STM_EVAL_LEDOn(LED4);

#ifdef USE_LCD
	/* Clear the LCD */
	LCD_Clear(Black);

	/* Set the LCD Back Color */
	LCD_SetBackColor(Black);

	/* Set the LCD Text Color */
	LCD_SetTextColor(White);

	/* Display message on the LCD*/
	LCD_DisplayStringLine(Line0, (uint8_t*) MESSAGE1);
	LCD_DisplayStringLine(Line1, (uint8_t*) MESSAGE2);
	LCD_DisplayStringLine(Line2, (uint8_t*) MESSAGE3);
	LCD_DisplayStringLine(Line3, (uint8_t*) MESSAGE4);
#endif
}
/**
 * @brief  Configures the ADC.
 * @param  None
 * @retval None
 */
static void ADC_Config(void) {
	ADC_InitTypeDef ADC_InitStructure;
	ADC_CommonInitTypeDef ADC_CommonInitStructure;
	GPIO_InitTypeDef GPIO_InitStructure;

	/* Enable ADC3 clock */
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_ADC3, ENABLE);

	/* GPIOF clock enable */
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOF, ENABLE);

	/* Configure ADC Channel7 as analog */
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_9;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AN;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_Init(GPIOF, &GPIO_InitStructure);

	/* ADC Common Init */
	ADC_CommonInitStructure.ADC_Mode = ADC_Mode_Independent;
	ADC_CommonInitStructure.ADC_Prescaler = ADC_Prescaler_Div6;
	ADC_CommonInitStructure.ADC_DMAAccessMode = ADC_DMAAccessMode_Disabled;
	ADC_CommonInitStructure.ADC_TwoSamplingDelay = ADC_TwoSamplingDelay_5Cycles;
	ADC_CommonInit(&ADC_CommonInitStructure);

	/* ADC3 Configuration ------------------------------------------------------*/
	ADC_StructInit(&ADC_InitStructure);
	ADC_InitStructure.ADC_Resolution = ADC_Resolution_8b;
	ADC_InitStructure.ADC_ScanConvMode = DISABLE;
	ADC_InitStructure.ADC_ContinuousConvMode = ENABLE;
	ADC_InitStructure.ADC_ExternalTrigConvEdge = ADC_ExternalTrigConvEdge_None;
	ADC_InitStructure.ADC_DataAlign = ADC_DataAlign_Right;
	ADC_InitStructure.ADC_NbrOfConversion = 1;
	ADC_Init(ADC3, &ADC_InitStructure);

	/* ADC3 Regular Channel Config */
	ADC_RegularChannelConfig(ADC3, ADC_Channel_7, 1, ADC_SampleTime_56Cycles);

	/* Enable ADC3 */
	ADC_Cmd(ADC3, ENABLE);

	/* ADC3 regular Software Start Conv */
	ADC_SoftwareStartConv(ADC3);
}

#ifdef  USE_FULL_ASSERT

/**
 * @brief  Reports the name of the source file and the source line number
 *   where the assert_param error has occurred.
 * @param  file: pointer to the source file name
 * @param  line: assert_param error line source number
 * @retval None
 */
void assert_failed(uint8_t* file, uint32_t line)
{
	/* User can add his own implementation to report the file name and line number,
	 ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

	/* Infinite loop */
	while (1)
	{}
}
#endif

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
