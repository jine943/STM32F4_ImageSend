################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../lwip_v1.3.2/src/core/ipv4/autoip.c \
../lwip_v1.3.2/src/core/ipv4/icmp.c \
../lwip_v1.3.2/src/core/ipv4/igmp.c \
../lwip_v1.3.2/src/core/ipv4/inet.c \
../lwip_v1.3.2/src/core/ipv4/inet_chksum.c \
../lwip_v1.3.2/src/core/ipv4/ip.c \
../lwip_v1.3.2/src/core/ipv4/ip_addr.c \
../lwip_v1.3.2/src/core/ipv4/ip_frag.c 

OBJS += \
./lwip_v1.3.2/src/core/ipv4/autoip.o \
./lwip_v1.3.2/src/core/ipv4/icmp.o \
./lwip_v1.3.2/src/core/ipv4/igmp.o \
./lwip_v1.3.2/src/core/ipv4/inet.o \
./lwip_v1.3.2/src/core/ipv4/inet_chksum.o \
./lwip_v1.3.2/src/core/ipv4/ip.o \
./lwip_v1.3.2/src/core/ipv4/ip_addr.o \
./lwip_v1.3.2/src/core/ipv4/ip_frag.o 

C_DEPS += \
./lwip_v1.3.2/src/core/ipv4/autoip.d \
./lwip_v1.3.2/src/core/ipv4/icmp.d \
./lwip_v1.3.2/src/core/ipv4/igmp.d \
./lwip_v1.3.2/src/core/ipv4/inet.d \
./lwip_v1.3.2/src/core/ipv4/inet_chksum.d \
./lwip_v1.3.2/src/core/ipv4/ip.d \
./lwip_v1.3.2/src/core/ipv4/ip_addr.d \
./lwip_v1.3.2/src/core/ipv4/ip_frag.d 


# Each subdirectory must supply rules for building sources it contributes
lwip_v1.3.2/src/core/ipv4/%.o: ../lwip_v1.3.2/src/core/ipv4/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo %cd%
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 -DSTM3240G_EVAL -DSTM32F407IGHx -DSTM32F4 -DSTM32 -DDEBUG -DUSE_STDPERIPH_DRIVER -DSTM32F40XX -DUSE_STM324xG_EVAL -DNO_SYS -DSTM32F40_41xxx -IC:/Ac6/Funspace/Lucky_1223/inc -IC:/Ac6/Funspace/Lucky_1223/lwip_v1.3.2/port/STM32 -IC:/Ac6/Funspace/Lucky_1223/lwip_v1.3.2/port/STM32/Standalone -IC:/Ac6/Funspace/Lucky_1223/lwip_v1.3.2/src/include/ipv4 -IC:/Ac6/Funspace/Lucky_1223/lwip_v1.3.2/src/include -IC:/Ac6/Funspace/Lucky_1223/lwip_v1.3.2/src/include/netif -IC:/Ac6/Funspace/Lucky_1223/CMSIS/core -IC:/Ac6/Funspace/Lucky_1223/CMSIS/device -IC:/Ac6/Funspace/Lucky_1223/fat_fs/inc -IC:/Ac6/Funspace/Lucky_1223/StdPeriph_Driver/inc -IC:/Ac6/Funspace/Lucky_1223/STM32F4x7_ETH_Driver/inc -IC:/Ac6/Funspace/Lucky_1223/Utilities/Common -IC:/Ac6/Funspace/Lucky_1223/Utilities/STM3240_41_G_EVAL -IC:/Ac6/Funspace/Lucky_1223/lwip_v1.3.2/src/core -O0 -g3 -Wall -fmessage-length=0 -ffunction-sections -c -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


