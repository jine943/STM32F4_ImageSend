################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../lwip_v1.3.2/src/core/dhcp.c \
../lwip_v1.3.2/src/core/dns.c \
../lwip_v1.3.2/src/core/init.c \
../lwip_v1.3.2/src/core/mem.c \
../lwip_v1.3.2/src/core/memp.c \
../lwip_v1.3.2/src/core/netif.c \
../lwip_v1.3.2/src/core/pbuf.c \
../lwip_v1.3.2/src/core/raw.c \
../lwip_v1.3.2/src/core/stats.c \
../lwip_v1.3.2/src/core/sys.c \
../lwip_v1.3.2/src/core/tcp.c \
../lwip_v1.3.2/src/core/tcp_in.c \
../lwip_v1.3.2/src/core/tcp_out.c \
../lwip_v1.3.2/src/core/udp.c 

OBJS += \
./lwip_v1.3.2/src/core/dhcp.o \
./lwip_v1.3.2/src/core/dns.o \
./lwip_v1.3.2/src/core/init.o \
./lwip_v1.3.2/src/core/mem.o \
./lwip_v1.3.2/src/core/memp.o \
./lwip_v1.3.2/src/core/netif.o \
./lwip_v1.3.2/src/core/pbuf.o \
./lwip_v1.3.2/src/core/raw.o \
./lwip_v1.3.2/src/core/stats.o \
./lwip_v1.3.2/src/core/sys.o \
./lwip_v1.3.2/src/core/tcp.o \
./lwip_v1.3.2/src/core/tcp_in.o \
./lwip_v1.3.2/src/core/tcp_out.o \
./lwip_v1.3.2/src/core/udp.o 

C_DEPS += \
./lwip_v1.3.2/src/core/dhcp.d \
./lwip_v1.3.2/src/core/dns.d \
./lwip_v1.3.2/src/core/init.d \
./lwip_v1.3.2/src/core/mem.d \
./lwip_v1.3.2/src/core/memp.d \
./lwip_v1.3.2/src/core/netif.d \
./lwip_v1.3.2/src/core/pbuf.d \
./lwip_v1.3.2/src/core/raw.d \
./lwip_v1.3.2/src/core/stats.d \
./lwip_v1.3.2/src/core/sys.d \
./lwip_v1.3.2/src/core/tcp.d \
./lwip_v1.3.2/src/core/tcp_in.d \
./lwip_v1.3.2/src/core/tcp_out.d \
./lwip_v1.3.2/src/core/udp.d 


# Each subdirectory must supply rules for building sources it contributes
lwip_v1.3.2/src/core/%.o: ../lwip_v1.3.2/src/core/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo %cd%
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 -DSTM3240G_EVAL -DSTM32F407IGHx -DSTM32F4 -DSTM32 -DDEBUG -DUSE_STDPERIPH_DRIVER -DSTM32F40XX -DUSE_STM324xG_EVAL -DNO_SYS -DSTM32F40_41xxx -IC:/Ac6/Funspace/Lucky_1223/inc -IC:/Ac6/Funspace/Lucky_1223/lwip_v1.3.2/port/STM32 -IC:/Ac6/Funspace/Lucky_1223/lwip_v1.3.2/port/STM32/Standalone -IC:/Ac6/Funspace/Lucky_1223/lwip_v1.3.2/src/include/ipv4 -IC:/Ac6/Funspace/Lucky_1223/lwip_v1.3.2/src/include -IC:/Ac6/Funspace/Lucky_1223/lwip_v1.3.2/src/include/netif -IC:/Ac6/Funspace/Lucky_1223/CMSIS/core -IC:/Ac6/Funspace/Lucky_1223/CMSIS/device -IC:/Ac6/Funspace/Lucky_1223/fat_fs/inc -IC:/Ac6/Funspace/Lucky_1223/StdPeriph_Driver/inc -IC:/Ac6/Funspace/Lucky_1223/STM32F4x7_ETH_Driver/inc -IC:/Ac6/Funspace/Lucky_1223/Utilities/Common -IC:/Ac6/Funspace/Lucky_1223/Utilities/STM3240_41_G_EVAL -IC:/Ac6/Funspace/Lucky_1223/lwip_v1.3.2/src/core -O0 -g3 -Wall -fmessage-length=0 -ffunction-sections -c -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


