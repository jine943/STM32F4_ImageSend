################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../lwip_v1.3.2/src/api/api_lib.c \
../lwip_v1.3.2/src/api/api_msg.c \
../lwip_v1.3.2/src/api/err.c \
../lwip_v1.3.2/src/api/netbuf.c \
../lwip_v1.3.2/src/api/netdb.c \
../lwip_v1.3.2/src/api/netifapi.c \
../lwip_v1.3.2/src/api/sockets.c \
../lwip_v1.3.2/src/api/tcpip.c 

OBJS += \
./lwip_v1.3.2/src/api/api_lib.o \
./lwip_v1.3.2/src/api/api_msg.o \
./lwip_v1.3.2/src/api/err.o \
./lwip_v1.3.2/src/api/netbuf.o \
./lwip_v1.3.2/src/api/netdb.o \
./lwip_v1.3.2/src/api/netifapi.o \
./lwip_v1.3.2/src/api/sockets.o \
./lwip_v1.3.2/src/api/tcpip.o 

C_DEPS += \
./lwip_v1.3.2/src/api/api_lib.d \
./lwip_v1.3.2/src/api/api_msg.d \
./lwip_v1.3.2/src/api/err.d \
./lwip_v1.3.2/src/api/netbuf.d \
./lwip_v1.3.2/src/api/netdb.d \
./lwip_v1.3.2/src/api/netifapi.d \
./lwip_v1.3.2/src/api/sockets.d \
./lwip_v1.3.2/src/api/tcpip.d 


# Each subdirectory must supply rules for building sources it contributes
lwip_v1.3.2/src/api/%.o: ../lwip_v1.3.2/src/api/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo %cd%
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 -DSTM3240G_EVAL -DSTM32F407IGHx -DSTM32F4 -DSTM32 -DDEBUG -DUSE_STDPERIPH_DRIVER -DSTM32F40XX -DUSE_STM324xG_EVAL -DNO_SYS -DSTM32F40_41xxx -IC:/Ac6/Funspace/Lucky_1223/inc -IC:/Ac6/Funspace/Lucky_1223/lwip_v1.3.2/port/STM32 -IC:/Ac6/Funspace/Lucky_1223/lwip_v1.3.2/port/STM32/Standalone -IC:/Ac6/Funspace/Lucky_1223/lwip_v1.3.2/src/include/ipv4 -IC:/Ac6/Funspace/Lucky_1223/lwip_v1.3.2/src/include -IC:/Ac6/Funspace/Lucky_1223/lwip_v1.3.2/src/include/netif -IC:/Ac6/Funspace/Lucky_1223/CMSIS/core -IC:/Ac6/Funspace/Lucky_1223/CMSIS/device -IC:/Ac6/Funspace/Lucky_1223/fat_fs/inc -IC:/Ac6/Funspace/Lucky_1223/StdPeriph_Driver/inc -IC:/Ac6/Funspace/Lucky_1223/STM32F4x7_ETH_Driver/inc -IC:/Ac6/Funspace/Lucky_1223/Utilities/Common -IC:/Ac6/Funspace/Lucky_1223/Utilities/STM3240_41_G_EVAL -IC:/Ac6/Funspace/Lucky_1223/lwip_v1.3.2/src/core -O0 -g3 -Wall -fmessage-length=0 -ffunction-sections -c -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


