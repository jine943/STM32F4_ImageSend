################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../src/camera_api.c \
../src/dcmi_ov2640.c \
../src/dcmi_ov9655.c \
../src/main.c \
../src/netconf.c \
../src/serial_debug.c \
../src/serial_io.c \
../src/stm32f4x7_eth_bsp.c \
../src/stm32f4xx_it.c \
../src/syscalls.c \
../src/system_stm32f4xx.c \
../src/udp_echoserver.c 

OBJS += \
./src/camera_api.o \
./src/dcmi_ov2640.o \
./src/dcmi_ov9655.o \
./src/main.o \
./src/netconf.o \
./src/serial_debug.o \
./src/serial_io.o \
./src/stm32f4x7_eth_bsp.o \
./src/stm32f4xx_it.o \
./src/syscalls.o \
./src/system_stm32f4xx.o \
./src/udp_echoserver.o 

C_DEPS += \
./src/camera_api.d \
./src/dcmi_ov2640.d \
./src/dcmi_ov9655.d \
./src/main.d \
./src/netconf.d \
./src/serial_debug.d \
./src/serial_io.d \
./src/stm32f4x7_eth_bsp.d \
./src/stm32f4xx_it.d \
./src/syscalls.d \
./src/system_stm32f4xx.d \
./src/udp_echoserver.d 


# Each subdirectory must supply rules for building sources it contributes
src/%.o: ../src/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo %cd%
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 -DSTM3240G_EVAL -DSTM32F407IGHx -DSTM32F4 -DSTM32 -DDEBUG -DUSE_STDPERIPH_DRIVER -DSTM32F40XX -DUSE_STM324xG_EVAL -DNO_SYS -DSTM32F40_41xxx -IC:/Ac6/Funspace/Lucky_1223/inc -IC:/Ac6/Funspace/Lucky_1223/lwip_v1.3.2/port/STM32 -IC:/Ac6/Funspace/Lucky_1223/lwip_v1.3.2/port/STM32/Standalone -IC:/Ac6/Funspace/Lucky_1223/lwip_v1.3.2/src/include/ipv4 -IC:/Ac6/Funspace/Lucky_1223/lwip_v1.3.2/src/include -IC:/Ac6/Funspace/Lucky_1223/lwip_v1.3.2/src/include/netif -IC:/Ac6/Funspace/Lucky_1223/CMSIS/core -IC:/Ac6/Funspace/Lucky_1223/CMSIS/device -IC:/Ac6/Funspace/Lucky_1223/fat_fs/inc -IC:/Ac6/Funspace/Lucky_1223/StdPeriph_Driver/inc -IC:/Ac6/Funspace/Lucky_1223/STM32F4x7_ETH_Driver/inc -IC:/Ac6/Funspace/Lucky_1223/Utilities/Common -IC:/Ac6/Funspace/Lucky_1223/Utilities/STM3240_41_G_EVAL -IC:/Ac6/Funspace/Lucky_1223/lwip_v1.3.2/src/core -O0 -g3 -Wall -fmessage-length=0 -ffunction-sections -c -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


