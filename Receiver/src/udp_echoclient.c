/**
 ******************************************************************************
 * @file    udp_echoclient.c
 * @author  MCD Application Team
 * @version V1.1.0
 * @date    31-July-2013
 * @brief   UDP echo client
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; COPYRIGHT 2013 STMicroelectronics</center></h2>
 *
 * Licensed under MCD-ST Liberty SW License Agreement V2, (the "License");
 * You may not use this file except in compliance with the License.
 * You may obtain a copy of the License at:
 *
 *        http://www.st.com/software_license_agreement_liberty_v2
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 ******************************************************************************
 */

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "lwip/pbuf.h"
#include "lwip/udp.h"
#include "lwip/tcp.h"
#include <string.h>
#include <stdio.h>
#include "serial_debug.h"
#include "udp_echoclient.h"
/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
#define SEND_SECTION_LEN		1000 // 30
#define TARGET_BUFFER		frame_buffer

/* Private macro -------------------------------------------------------------*/

/* Private variables ---------------------------------------------------------*/
char m_client_state;
uint16_t frame_buffer[120 * 160] = { 0, };

unsigned int g_frame_total_len;
unsigned short g_cur_section;
unsigned short g_total_section;
unsigned int g_recv_frame_count;

u8_t data[100];
__IO uint32_t message_count = 0;
// uint16_t test_buffer[100] = {0, };

/* Private function prototypes -----------------------------------------------*/
void WriteFrameToLCD(void);

/* Private functions ---------------------------------------------------------*/

/**
 * @brief  Connect to UDP echo server
 * @param  None
 * @retval None
 */
void udp_echoclient_connect(void) {
	struct udp_pcb *upcb;
	struct pbuf *p;
	struct ip_addr DestIPaddr;
	err_t err;

	if (UN_CONNECT == m_client_state) {
		/* Create a new UDP control block  */
		upcb = udp_new();

		if (upcb != NULL) {
			m_client_state = UN_CONNECT;

			/*assign destination IP address */
			IP4_ADDR(&DestIPaddr, DEST_IP_ADDR0, DEST_IP_ADDR1, DEST_IP_ADDR2,
					DEST_IP_ADDR3);

			err = udp_bind(upcb, IP_ADDR_ANY, UDP_CLIENT_PORT);

			if (ERR_OK == err) {
				/* configure destination IP address and port */
				err = udp_connect(upcb, &DestIPaddr, UDP_SERVER_PORT);

				if (err == ERR_OK) {
					printf2("[C] 1st send:");
					/* Set a receive callback for the upcb */
					udp_recv(upcb, udp_receive_callback, NULL);

					memcpy(data, "CONNECT", sizeof("CONNECT"));
					// sprintf((char*)data, "sending udp client message %d", (int*)message_count);

					/* allocate pbuf from pool*/
					p = pbuf_alloc(PBUF_TRANSPORT, strlen((char*) data),
							PBUF_POOL);

					if (p != NULL) {
						/* copy data to pbuf */
						pbuf_take(p, (char*) data, strlen((char*) data));

						printf2("%s\n", p->payload);
						/* send udp data */
						udp_send(upcb, p);

						/* free pbuf */
						pbuf_free(p);
					} else {
						/* free the UDP connection, so we can accept new clients */
						udp_remove(upcb);
#ifdef SERIAL_DEBUG
						printf2("\n\r can not allocate pbuf ");
#endif
					}
				} else {
					/* free the UDP connection, so we can accept new clients */
					udp_remove(upcb);
#ifdef SERIAL_DEBUG
					printf2("\n\r can not connect udp pcb");
#endif
				}
			} // bind..

		} else {
#ifdef SERIAL_DEBUG
			printf2("\n\r can not create udp pcb");
#endif
		}
	} else
		print_status();
}

/**
 * @brief This function is called when an UDP datagrm has been received on the port UDP_PORT.
 * @param arg user supplied argument (udp_pcb.recv_arg)
 * @param pcb the udp_pcb which received data
 * @param p the packet buffer that was received
 * @param addr the remote IP address from which the packet was received
 * @param port the remote port from which the packet was received
 * @retval None
 * sizeof count until "NULL"
 */
void udp_receive_callback(void *arg, struct udp_pcb *upcb, struct pbuf *p,
		struct ip_addr *addr, u16_t port) {
	char *recv_data = p->payload;
	struct pbuf *new_buf;
	int i;
	char req_img_msg[20];
	memcpy(req_img_msg, "REQUEST_IMAGE", sizeof("REQUEST_IMAGE"));

	// printf2("[C] recv : %s, tot_len %d\n", recv_data, p->tot_len);

	switch (m_client_state) {

	case UN_CONNECT:
		if (0 == memcmp(recv_data, "CONNECT", sizeof("CONNECT"))) {
			m_client_state = CONNECT;
			printf2("[C] 2st send: CAMERA ON");

			pbuf_free(p);

			memcpy(data, "CAMERA_ON", sizeof("CAMERA_ON"));
			new_buf = pbuf_alloc(PBUF_TRANSPORT, sizeof(data), PBUF_RAM);
			pbuf_take(new_buf, data, sizeof(data));

			// printf2("[C]new buf : %s \n", new_buf->payload);

			udp_sendto(upcb, new_buf, addr, UDP_SERVER_PORT);
			pbuf_free(new_buf);
		} else {
			set_status(upcb, UN_CONNECT);
		}
		break;
	case CONNECT:
		if (0 == memcmp(recv_data, "CAMERA_ON", sizeof("CAMERA_ON"))) {
			m_client_state = REQUEST_IMAGE;
			pbuf_free(p);

			// print_RGB(0, 10);
			Init_image_recv_process();

			new_buf = pbuf_alloc(PBUF_TRANSPORT, sizeof(req_img_msg), PBUF_RAM);
			pbuf_take(new_buf, req_img_msg, sizeof(req_img_msg));

			printf2("[C]new buf : %s \n", new_buf->payload);

			udp_sendto(upcb, new_buf, addr, UDP_SERVER_PORT);
			pbuf_free(new_buf);
		} else {
			set_status(upcb, UN_CONNECT);
		}

		break;
	case REQUEST_IMAGE:
		process_udp_data(p);
		pbuf_free(p);

		/*printf2("[C]%d sec config **********\n", g_cur_section);

		 for(i = g_cur_section * SEND_SECTION_LEN/2;
		 i < ((1+g_cur_section) * SEND_SECTION_LEN/2); i += 200){
		 print_RGB(i, i+5);
		 }

		 printf2("[C]%d sec config end**********\n\n\n", g_cur_section);
		 printf2("next section require\n\n");
		 */

		if (g_total_section - 1 == g_cur_section) {
			// Last section
			// printf2("\n %d Frame!!!!", g_recv_frame_count++);
			// print_RGB(0, 10);
			g_cur_section = 0;

			// write 1 frame that received to LCD
			Write_Raw_Data_To_LCD(TARGET_BUFFER, sizeof(TARGET_BUFFER) / sizeof(uint16_t));
		}
		// is not last section
		new_buf = pbuf_alloc(PBUF_TRANSPORT, sizeof(req_img_msg), PBUF_RAM);
		pbuf_take(new_buf, req_img_msg, sizeof(req_img_msg));

		// printf2("[C]send buf %s\n", new_buf->payload);

		udp_sendto(upcb, new_buf, addr, UDP_SERVER_PORT);
		pbuf_free(new_buf);

		break;
	default:
		break;
	}

	/* Free receive pbuf */
	if (NULL != p)
		pbuf_free(p);
}



/*
 *
 */
void process_udp_data(struct pbuf *p_buf) {
	int i;
	char *a_data = (char *) p_buf->payload;
	unsigned int len = p_buf->len;
	unsigned char c;
	static int main_pkt_sta = 0;
	// static int main_pkt_cmd = 0;
	static unsigned short recv_data_len;
	char *p_pos;

	for (i = 0; i < len; i++) {
		c = a_data[i];
		switch (main_pkt_sta) {
		case 0:
			if (c == 'J')
				main_pkt_sta++;
			else
				main_pkt_sta = 0;
			break;

		case 1:
			if (c == 'H')
				main_pkt_sta++;
			else
				main_pkt_sta = 0;
			break;

		case 2:
			if (c == 'P')
				main_pkt_sta++;
			else
				main_pkt_sta = 0;
			break;

		case 3:
			recv_data_len = 0;
			recv_data_len = c;
			main_pkt_sta++;
			break;

		case 4:
			// recv_data_len = recv_data_len << 8;
			recv_data_len = recv_data_len | (c << 8);
			recv_data_len ^= 0xffff;
			// printf2("recv len : %d\n", recv_data_len);
			main_pkt_sta++;
			break;

		case 5:
			g_cur_section = 0;
			g_cur_section = c;
			main_pkt_sta++;
			break;

		case 6:
			// g_cur_section = g_cur_section << 8;
			g_cur_section = g_cur_section | (c << 8);
			g_cur_section ^= 0xffff;
			// printf2("cur_sec : %d\n", g_cur_section);
			main_pkt_sta++;
			break;

		case 7:
			g_total_section = 0;
			g_total_section = c;
			main_pkt_sta++;
			break;

		case 8:
			// g_total_section = g_total_section << 8;
			g_total_section = g_total_section | (c << 8);
			g_total_section ^= 0xffff;
			// printf2("tot_sec : %x, %d\n", g_total_section, g_total_section);
			main_pkt_sta++;
			break;

		case 9:
			p_pos = TARGET_BUFFER + SEND_SECTION_LEN / 2 * g_cur_section;
			// memcpy(p_pos, &a_data[i], len - HEAD_PACKET_LEN);
			// p_pos += (len - HEAD_PACKET_LEN);
			pbuf_copy_partial(p_buf, p_pos, recv_data_len, HEAD_PACKET_LEN);
			main_pkt_sta = 0;
			return;
			break;

		}
	}
}



/*
 * Initial variable related receiving image
 */
void Init_image_recv_process() {
	g_cur_section = 0;
	g_total_section = g_frame_total_len / SEND_SECTION_LEN;

	if (0 != g_frame_total_len % SEND_SECTION_LEN)
		g_total_section++;

	g_frame_total_len = sizeof(TARGET_BUFFER);

	Demo_LCD_Clear();
}


/*
 * set m_client_status
 * param upcb the handle of udp
 * param a_status the status that u want to set like UN_CONNECT or ...
 */
void set_status(struct udp_pcb *upcb, char a_status) {
	if (UN_CONNECT == a_status) {
		m_client_state = UN_CONNECT;
		/* free the UDP connection, so we can accept new clients */
		udp_remove(upcb);
		printf2("[C] : UN_CONNECT\n\n\n");
	}
}


/*
 * Write frame to LCD
 * param p_raw_data the raw data (RGB, 565) 2byte data
 * param data_count the count of raw data that is sizeof(p_raw_data) / sizeof(uint16_t)
 */
void Write_Raw_Data_To_LCD(uint16_t *p_raw_data, uint32_t data_count){
	uint32_t i = 0, pixel = data_count;

	// Show image selected area
	LCD_SetDisplayWindow(179, 239, 120, 160);
	LCD_WriteReg(LCD_REG_3, 0x1008);

	LCD_WriteRAM_Prepare();

	// must write 76800(320*240)pixel to show image
	for (i = 0; i < 76800; i++) // QVGA(320*240)
	{
		LCD_WriteRAM(p_raw_data[i % pixel]); // QQVGA First 320*240�� ��ȿ
	}

	LCD_WriteReg(LCD_REG_3, 0x1018);
}



/*
 * print m_client_status to USART
 */
void print_status() {
	switch (m_client_state) {
	case UN_CONNECT:
		printf2("[C] : UN_CONNECT\n");
		break;
	case CONNECT:
		printf2("[C] : CONNECT\n");
		break;
	case REQUEST_IMAGE:
		printf2("[C] : REQUEST_IMAGE\n");
		break;
	default:
		break;
	}
}

/*
 * print Raw data(uint16_t) to RGB
 */
void print_RGB(int start_pixel, int dest_pixel) {
	char r = 0, g = 0, b = 0;
	uint16_t pixel;

	// R(5) G(6) B(5)
	if (start_pixel >= 0 && dest_pixel < 120 * 160
			&& (dest_pixel - start_pixel) > 0) {
		printf2("\n*[%d : %d]", start_pixel, dest_pixel);

		int i;
		for (i = start_pixel; i < dest_pixel; i++) {
			pixel = TARGET_BUFFER[i];
			r = (pixel & 0xF800) >> 11;
			g = (pixel & 0x7E0) >> 5;
			b = pixel & 0x001F;
			printf2("%d.%d.%d/", r, g, b);
		}
		Delay(600);
	}
}

/*
 * Demo LCD Clear
 */
void Demo_LCD_Clear(void) {
	uint32_t j;
	for (j = 0; j < 19; j++) {
		LCD_ClearLine(LINE(j));
	}
}

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
