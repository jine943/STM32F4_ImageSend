################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Utilities/STM3240_41_G_EVAL/stm324xg_eval.c \
../Utilities/STM3240_41_G_EVAL/stm324xg_eval_fsmc_sram.c \
../Utilities/STM3240_41_G_EVAL/stm324xg_eval_ioe.c \
../Utilities/STM3240_41_G_EVAL/stm324xg_eval_lcd.c \
../Utilities/STM3240_41_G_EVAL/stm324xg_eval_sdio_sd.c 

OBJS += \
./Utilities/STM3240_41_G_EVAL/stm324xg_eval.o \
./Utilities/STM3240_41_G_EVAL/stm324xg_eval_fsmc_sram.o \
./Utilities/STM3240_41_G_EVAL/stm324xg_eval_ioe.o \
./Utilities/STM3240_41_G_EVAL/stm324xg_eval_lcd.o \
./Utilities/STM3240_41_G_EVAL/stm324xg_eval_sdio_sd.o 

C_DEPS += \
./Utilities/STM3240_41_G_EVAL/stm324xg_eval.d \
./Utilities/STM3240_41_G_EVAL/stm324xg_eval_fsmc_sram.d \
./Utilities/STM3240_41_G_EVAL/stm324xg_eval_ioe.d \
./Utilities/STM3240_41_G_EVAL/stm324xg_eval_lcd.d \
./Utilities/STM3240_41_G_EVAL/stm324xg_eval_sdio_sd.d 


# Each subdirectory must supply rules for building sources it contributes
Utilities/STM3240_41_G_EVAL/%.o: ../Utilities/STM3240_41_G_EVAL/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo %cd%
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 -DSTM3240G_EVAL -DSTM32F407IGHx -DSTM32F4 -DSTM32 -DDEBUG -DUSE_STDPERIPH_DRIVER -DSTM32F40XX -DUSE_STM324xG_EVAL -DNO_SYS -I"C:/Ac6/Funspace/Happy_1224/inc" -I"C:/Ac6/Funspace/Happy_1224/lwip_v1.3.2/port/STM32" -I"C:/Ac6/Funspace/Happy_1224/lwip_v1.3.2/port/STM32/Standalone" -I"C:/Ac6/Funspace/Happy_1224/lwip_v1.3.2/src/include/ipv4" -I"C:/Ac6/Funspace/Happy_1224/lwip_v1.3.2/src/include" -I"C:/Ac6/Funspace/Happy_1224/lwip_v1.3.2/src/include/netif" -I"C:/Ac6/Funspace/Happy_1224/CMSIS/core" -I"C:/Ac6/Funspace/Happy_1224/CMSIS/device" -I"C:/Ac6/Funspace/Happy_1224/fat_fs/inc" -I"C:/Ac6/Funspace/Happy_1224/StdPeriph_Driver/inc" -I"C:/Ac6/Funspace/Happy_1224/STM32F4x7_ETH_Driver/inc" -I"C:/Ac6/Funspace/Happy_1224/Utilities/Common" -I"C:/Ac6/Funspace/Happy_1224/Utilities/STM3240_41_G_EVAL" -I"C:/Ac6/Funspace/Happy_1224/lwip_v1.3.2/src/core" -O0 -g3 -Wall -fmessage-length=0 -ffunction-sections -c -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


