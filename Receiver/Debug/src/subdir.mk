################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../src/main.c \
../src/netconf.c \
../src/serial_debug.c \
../src/serial_io.c \
../src/stm32f4x7_eth_bsp.c \
../src/stm32f4xx_it.c \
../src/syscalls.c \
../src/system_stm32f4xx.c \
../src/udp_echoclient.c 

OBJS += \
./src/main.o \
./src/netconf.o \
./src/serial_debug.o \
./src/serial_io.o \
./src/stm32f4x7_eth_bsp.o \
./src/stm32f4xx_it.o \
./src/syscalls.o \
./src/system_stm32f4xx.o \
./src/udp_echoclient.o 

C_DEPS += \
./src/main.d \
./src/netconf.d \
./src/serial_debug.d \
./src/serial_io.d \
./src/stm32f4x7_eth_bsp.d \
./src/stm32f4xx_it.d \
./src/syscalls.d \
./src/system_stm32f4xx.d \
./src/udp_echoclient.d 


# Each subdirectory must supply rules for building sources it contributes
src/%.o: ../src/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo %cd%
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 -DSTM3240G_EVAL -DSTM32F407IGHx -DSTM32F4 -DSTM32 -DDEBUG -DUSE_STDPERIPH_DRIVER -DSTM32F40XX -DUSE_STM324xG_EVAL -DNO_SYS -I"C:/Ac6/Funspace/Happy_1224/inc" -I"C:/Ac6/Funspace/Happy_1224/lwip_v1.3.2/port/STM32" -I"C:/Ac6/Funspace/Happy_1224/lwip_v1.3.2/port/STM32/Standalone" -I"C:/Ac6/Funspace/Happy_1224/lwip_v1.3.2/src/include/ipv4" -I"C:/Ac6/Funspace/Happy_1224/lwip_v1.3.2/src/include" -I"C:/Ac6/Funspace/Happy_1224/lwip_v1.3.2/src/include/netif" -I"C:/Ac6/Funspace/Happy_1224/CMSIS/core" -I"C:/Ac6/Funspace/Happy_1224/CMSIS/device" -I"C:/Ac6/Funspace/Happy_1224/fat_fs/inc" -I"C:/Ac6/Funspace/Happy_1224/StdPeriph_Driver/inc" -I"C:/Ac6/Funspace/Happy_1224/STM32F4x7_ETH_Driver/inc" -I"C:/Ac6/Funspace/Happy_1224/Utilities/Common" -I"C:/Ac6/Funspace/Happy_1224/Utilities/STM3240_41_G_EVAL" -I"C:/Ac6/Funspace/Happy_1224/lwip_v1.3.2/src/core" -O0 -g3 -Wall -fmessage-length=0 -ffunction-sections -c -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


