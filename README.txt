================================================================
       전장(차량)용 ECM(CPU) 기반 카메라 영상 전송 시스템
=================================================================
1) 진행기간 : 2015.09 - 2016.01

2) 주요내용 : 전장(차량)용 ECM(CPU) 기반 카메라 영상 전송 시스템

3) 사용언어 : C 언어

4) 개발 툴  : SW4STM (무료 개발 툴) 

5)특징
  - LwIP(Light weight TCP/IP) 통신을 적용한 메모리 문제 해결
  - CPU 처리 속도 차이에 따른 프레임 누락 문제를 DMA(Direct Memory Access)를 통해 해결
  - 프레임 100 분할 전송 알고리즘 구상/적용

6)사용한 Skill 또는 지식 
    LwIP(Light weight TCP/IP), DMA, STM32F4 CPU 개발을 위한 크로스플랫폼 SW4STM32 사용
    
    